<?php
session_start();
?>

<?php
include '../include/head.php';
?>

<div class="container">
    <h2 style="text-align:center">Login Info</h2>
    <form class="form-horizontal" action="../backend/dashboard.php" method="post">


        <div class="col-md-offset-4 col-md-4">
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
            </div>
            <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
            </div>
            <button type="submit" class="btn btn-default">Login</button>
        </div>
    </form>
</div>

<?php
include '../include/foter.php';
?>
