<?php
session_start();
?>
<?php
include '../include/head.php';
?>
    <div class="container">
        <h2 style="text-align:center">Add User</h2>
        <form class="form-horizontal" action="../register.php" method="post">
            <div class="form-group">
                <label class="control-label col-sm-2" for="name">Name:</label>
                <div class="col-sm-10">
                    <input type="text"
                           class="form-control" id="name"
                           placeholder="Enter name" name="name">
                    <?php
                    if (isset($_SESSION['name'])) {
                        echo $_SESSION['name'];
                        unset ($_SESSION['name']);
                    }
                    ?>

                </div>
            </div>
            <br>

            <div class="form-group">
                <label class="control-label col-sm-2" for="email">Email:</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">

                    <?php
                    if (isset($_SESSION['email exist'])) {
                        echo $_SESSION['email exist'];
                        unset ($_SESSION['email exist']);
                    }
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="password">Password:</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="phone">Phone:</label>
                <div class="col-sm-10">
                    <input type="phone" class="form-control" id="phone" placeholder="Enter phone number" name="phone">
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-sm-2" for="gender">Gender:</label>
                <div class="col-sm-10">
                    <br>
                    <input type="radio" name="gender" value="male">Male
                    <input type="radio" name="gender" value="female">Female
                    <input type="radio" name="gender" value="others">Others

                </div>
            </div>


            <div class="form-group text-center">
                <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
    </div>
    </div>
    </form>
    </div>
<?php
include '../footer.php';
?>