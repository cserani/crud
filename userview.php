<?php
session_start();
require '../db.php';
$select ="SELECT * FROM practice";
$query =mysqli_query($db,$select);
?>
<?php
include '../include/head.php';
?>


<div class="container">
    <h2 style="text-align:center">USER VIEW</h2>


    <?php

    if (isset($_SESSION['delete'])){
        ?>
        <div  class="alert alert-success">
            <p><?=  $_SESSION['delete'];?></p>
        </div>

        <?php
        unset($_SESSION['delete']);
    }


    ?>
    <table id="mytable" class="table">

        <thead class="thead-dark">
        <tr>

            <th>SL</th>
            <th>NAME</th>
            <th>EMAIL</th>
            <th>PHONE</th>
            <th>ACTION</th>
        </tr>
        </thead>




        <tbody>
        <?php
        foreach($query as $key=>$value){
            ?>

        <tr>
            <td><?=++$key?></td>
            <td><?php echo $value['Name']?></td>
            <td><?= $value['Email'] ?></td>
            <td><?=$value['Phone']?></td>
            <td class="center">
                <a href="user-edit.php?id=<?= $value['Id']?>" class="btn btn-primary">Edit</a>
                <a href=" user-delete.php?id=<?php
                echo $value['Id']?>"  class="btn btn-danger">Delete</a>
            </td>
        </tr>

        <?php
            }

         ?>


        </tbody>
</div>
<?php
include '../footer.php';
?>
<script type="text/javascript">
    $(document).ready( function () {
        $('#mytable').DataTable();
    } );
</script>
