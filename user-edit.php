<?php
session_start();
require '../db.php';
$id = $_GET['id'];
$data= "SELECT * FROM practice WHERE Id='$id'";

$query= mysqli_query($db,$data);
$practicesdata= mysqli_fetch_assoc($query);

?>

<?php
include '../include/head.php';
?>


<div class="container">
    <h2 style="text-align:center">UPDATE DATA<?php echo $_GET['id'];?></h2>
    <form class="form-horizontal" action="upd.php" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2" for="name">Name:</label>
            <div class="col-sm-10">
                <input type="text" value="<?php echo $id?>" name="id" hidden>
                <?php

                // $border_color= ;
                ?>
                <input type="text"
                       class="form-control" id="name"
                       placeholder="Enter name" name="name" value="<?php echo $practicesdata['Name'];?>"
                >

                <?php

                if (isset($_SESSION['$name'])){
                    echo $_SESSION['$name'];

                    unset($_SESSION['$name']);
                }


                ?>

            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" value="<?php echo $practicesdata['Email'];?> " placeholder="Enter email" name="email">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="pwd">Password:</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="pwd" value="<?php echo $practicesdata['Password'];?>" placeholder="Enter password" name="pwd">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="phone">Phone:</label>
            <div class="col-sm-10">
                <input type="phone" class="form-control" id="phone" value="<?php echo $practicesdata['Phone'];?>" placeholder="Enter phone number" name="phone">
            </div>
        </div>

        <div class="form-group" text-center>
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default text-center">Submit</button>
            </div>
        </div>
    </form>
</div>

</body>


</html>
